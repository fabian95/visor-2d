function inimap(){
    var mymap = L.map('visor').setView([4.618167, -74.137284], 16);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 20,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZmFiaWFuLXNhbmFicmlhIiwiYSI6ImNrZG54ZmVkbTBnZTIycm95Z2M1aWtjZTkifQ.9Fo_zITkr3HYVakt4tDVZw'
}).addTo(mymap);

var marker = L.marker([4.618361, -74.136377]).addTo(mymap);
var marker = L.marker([4.630953, -74.157794]).addTo(mymap);
var marker = L.marker([4.629147, -74.172901]).addTo(mymap);
var marker = L.marker([4.616488, -74.153686]).addTo(mymap);

var circle = L.circle([4.631306, -74.148278], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.2,
    radius: 500
}).addTo(mymap);

var polygon = L.polygon([
    [4.618361, -74.136377],
    [4.630953, -74.157794],
    [4.629147, -74.172901],
    [4.616488, -74.153686]
]).addTo(mymap);

marker.bindPopup("sitios de interes").openPopup();
circle.bindPopup("monumento de banderas");
polygon.bindPopup("I am a polygon.");

}